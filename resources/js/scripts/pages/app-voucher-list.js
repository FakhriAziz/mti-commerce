/*=========================================================================================
    File Name: app-invoice-list.js
    Description: app-invoice-list Javascripts
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
   Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {
    'use strict';
  
    var dtInvoiceTable = $('.voucher-list-table'),
      assetPath = '../../../app-assets/',
      invoicePreview = 'app-voucher-preview.html',
      voucherAdd = 'app-voucher-buy.html',
      invoiceEdit = 'app-voucher-edit.html';
  
    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
      invoicePreview = assetPath + 'app/voucher/preview';
      voucherAdd = assetPath + 'app/voucher/buy';
      invoiceEdit = assetPath + 'app/voucher/edit';
    }
  
    // datatable
    if (dtInvoiceTable.length) {
      var dtInvoice = dtInvoiceTable.DataTable({
        ajax: assetPath + 'data/voucher-list.json', // JSON file to add data
        autoWidth: false,
        columns: [
          // columns according to JSON
          { data: 'voucher_id' },
          { data: 'voucher_status' },
          { data: 'issued_date' },
          { data: 'voucher_status' },
          { data: '' }
        ],
        columnDefs: [
          {
            // Voucher ID
            targets: 0,
            width: '150px',
            render: function (data, type, full, meta) {
              var $invoiceId = full['voucher_id'];
              // Creates full output for row
              var $rowOutput = '<a class="font-weight-bold" href="' + invoicePreview + '"> #' + $invoiceId + '</a>';
              return $rowOutput;
            }
          },
          {
            // Voucher status
            targets: 1,
            width: '150px',
            render: function (data, type, full, meta) {
              var $status = full['voucher_status'],
                $dueDate = full['due_date'],
                $balance = full['balance'],
                roleObj = {
                  Active: { class: 'bg-light-success', icon: 'check-circle' },
                  Expired: { class: 'bg-light-danger', icon: 'info' },
                  'Partial Payment': { class: 'bg-light-warning', icon: 'pie-chart' }
                };
              return (
                "<span data-toggle='tooltip' data-html='true' title='<span>" +
                $status +
                '<br> <span class="font-weight-bold">Balance:</span> ' +
                $balance +
                '<br> <span class="font-weight-bold">Due Date:</span> ' +
                $dueDate +
                "</span>'>" +
                '<div class="avatar avatar-status ' +
                roleObj[$status].class +
                '">' +
                '<span class="avatar-content">' +
                feather.icons[roleObj[$status].icon].toSvg({ class: 'avatar-icon' }) +
                '</span>' +
                '</div>' +
                '</span>'
              );
            }
          },
          {
            targets: 3,
            visible: false
          },
          {
            // Actions
            targets: -1,
            title: 'Actions',
            width: '80px',
            orderable: false,
            render: function (data, type, full, meta) {
              return (
                '<div class="d-flex align-items-center col-actions">' +
                '<a class="mr-1" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Send Mail">' +
                feather.icons['send'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<a class="mr-1" href="' +
                invoicePreview +
                '" data-toggle="tooltip" data-placement="top" title="Preview Invoice">' +
                feather.icons['eye'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<div class="dropdown">' +
                '<a class="btn btn-sm btn-icon px-0" data-toggle="dropdown">' +
                feather.icons['more-vertical'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<div class="dropdown-menu dropdown-menu-right">' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['download'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Download</a>' +
                '<a href="' +
                invoiceEdit +
                '" class="dropdown-item">' +
                feather.icons['edit'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Edit</a>' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['trash'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Delete</a>' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Duplicate</a>' +
                '</div>' +
                '</div>' +
                '</div>'
              );
            }
          }
        ],
        order: [[1, 'desc']],
        dom:
          '<"row d-flex justify-content-between align-items-center m-1"' +
          '<"col-lg-6 d-flex align-items-center"l<"dt-action-buttons text-xl-right text-lg-left text-lg-right text-left "B>>' +
          '<"col-lg-6 d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap pr-lg-1 p-0"f<"voucher_status ml-sm-2">>' +
          '>t' +
          '<"d-flex justify-content-between mx-2 row"' +
          '<"col-sm-12 col-md-6"i>' +
          '<"col-sm-12 col-md-6"p>' +
          '>',
        language: {
          sLengthMenu: 'Show _MENU_',
          search: 'Cari',
          searchPlaceholder: 'Cari Voucher',
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        },
        // Buttons with Dropdown
        buttons: [
          {
            text: 'Beli Voucher',
            className: 'btn btn-primary btn-add-record ml-2',
            action: function (e, dt, button, config) {
              window.location = voucherAdd;
            }
          }
        ],
        // For responsive popup
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Details of ' + data['client_name'];
              }
            }),
            type: 'column',
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: 'table',
              columnDefs: [
                {
                  targets: 2,
                  visible: false
                },
                {
                  targets: 3,
                  visible: false
                }
              ]
            })
          }
        },
        initComplete: function () {
          $(document).find('[data-toggle="tooltip"]').tooltip();
          // Adding role filter once table initialized
          this.api()
            .columns(3)
            .every(function () {
              var column = this;
              var select = $(
                '<select id="UserRole" class="form-control ml-50 text-capitalize"><option value=""> Pilih Status </option></select>'
              )
                .appendTo('.voucher_status')
                .on('change', function () {
                  var val = $.fn.dataTable.util.escapeRegex($(this).val());
                  column.search(val ? '^' + val + '$' : '', true, false).draw();
                });
  
              column
                .data()
                .unique()
                .sort()
                .each(function (d, j) {
                  select.append('<option value="' + d + '" class="text-capitalize">' + d + '</option>');
                });
            });
        },
        drawCallback: function () {
          $(document).find('[data-toggle="tooltip"]').tooltip();
        }
      });
    }
  });
  