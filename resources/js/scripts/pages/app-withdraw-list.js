/*=========================================================================================
    File Name: app-withdraw-list.js
    Description: app-withdraw-list Javascripts
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
   Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {
    'use strict';
  
    var dtWithdrawTable = $('.withdraw-list-table'),
      assetPath = '../../../app-assets/',
      withdrawPreview = 'app-withdraw-preview.html',
      withdrawAdd = 'app-withdraw-buy.html',
      withdrawEdit = 'app-withdraw-edit.html';
  
    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
      withdrawPreview = assetPath + 'app/withdraw/preview';
      withdrawAdd = assetPath + 'app/withdraw/buy';
      withdrawEdit = assetPath + 'app/withdraw/edit';
    }
  
    // datatable
    if (dtWithdrawTable.length) {
      var dtWithdraw = dtWithdrawTable.DataTable({
        ajax: assetPath + 'data/withdraw-list.json', // JSON file to add data
        autoWidth: false,
        columns: [
          // columns according to JSON
          { data: 'withdraw_id' },
          { data: 'issued_date' },
          { data: 'total' },
          { data: 'withdraw_status' },
          { data: 'withdraw_status' }, //hidden for filter
          { data: '' }
        ],
        columnDefs: [
          {
            // Voucher ID
            targets: 0,
            width: '150px',
            render: function (data, type, full, meta) {
              var $withdrawId = full['withdraw_id'];
              // Creates full output for row
              var $rowOutput = '<a class="font-weight-bold" href="' + withdrawPreview + '"> #' + $withdrawId + '</a>';
              return $rowOutput;
            }
          },
          {
            // Total Invoice Amount
            targets: 2,
            width: '73px',
            render: function (data, type, full, meta) {
              var $total = full['total'];
              return '<span class="d-none">' + $total + '</span>Rp.' + $total;
            }
          },
          {
            // Voucher status
            targets: 3,
            width: '150px',
            render: function (data, type, full, meta) {
              var $status = full['withdraw_status'],
                $dueDate = full['due_date'],
                $balance = full['balance'],
                roleObj = {
                  'Sudah Ditarik': { class: 'bg-light-success', icon: 'check-circle' },
                  'Belum Ditarik': { class: 'bg-light-warning', icon: 'pie-chart' }
                };
              return (
                "<span data-toggle='tooltip' data-html='true' title='<span>" +
                $status +
                '<br> <span class="font-weight-bold">Balance:</span> ' +
                $balance +
                '<br> <span class="font-weight-bold">Due Date:</span> ' +
                $dueDate +
                "</span>'>" +
                '<div class="avatar avatar-status ' +
                roleObj[$status].class +
                '">' +
                '<span class="avatar-content">' +
                feather.icons[roleObj[$status].icon].toSvg({ class: 'avatar-icon' }) +
                '</span>' +
                '</div>' +
                '</span>'
              );
            }
          },
          {
            targets: 4,
            visible: false
          },
          {
            // Actions
            targets: -1,
            title: 'Actions',
            width: '80px',
            orderable: false,
            render: function (data, type, full, meta) {
              return (
                '<div class="d-flex align-items-center col-actions">' +
                '<a class="mr-1" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Send Mail">' +
                feather.icons['send'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<a class="mr-1" href="' +
                withdrawPreview +
                '" data-toggle="tooltip" data-placement="top" title="Preview Bonus">' +
                feather.icons['eye'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<div class="dropdown">' +
                '<a class="btn btn-sm btn-icon px-0" data-toggle="dropdown">' +
                feather.icons['more-vertical'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<div class="dropdown-menu dropdown-menu-right">' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['download'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Download</a>' +
                '<a href="' +
                withdrawEdit +
                '" class="dropdown-item">' +
                feather.icons['edit'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Edit</a>' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['trash'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Delete</a>' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Duplicate</a>' +
                '</div>' +
                '</div>' +
                '</div>'
              );
            }
          }
        ],
        order: [[1, 'desc']],
        dom:
          '<"row d-flex justify-content-between align-items-center m-1"' +
          '<"col-lg-6 d-flex align-items-center"l<"dt-action-buttons text-xl-right text-lg-left text-lg-right text-left "B>>' +
          '<"col-lg-6 d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap pr-lg-1 p-0"f<"voucher_status ml-sm-2">>' +
          '>t' +
          '<"d-flex justify-content-between mx-2 row"' +
          '<"col-sm-12 col-md-6"i>' +
          '<"col-sm-12 col-md-6"p>' +
          '>',
        language: {
          sLengthMenu: 'Show _MENU_',
          search: 'Cari',
          searchPlaceholder: 'Cari Data',
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        },
        // Buttons with Dropdown
        buttons: [
          {
            text: 'Beli Voucher',
            className: 'btn btn-primary btn-add-record ml-2',
            // action: function (e, dt, button, config) {
            //   window.location = voucherAdd;
            // }
          }
        ],
        // For responsive popup
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Details of ' + data['client_name'];
              }
            }),
            type: 'column',
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: 'table',
              columnDefs: [
                {
                  targets: 2,
                  visible: false
                },
                {
                  targets: 3,
                  visible: false
                }
              ]
            })
          }
        },
        initComplete: function () {
          $(document).find('[data-toggle="tooltip"]').tooltip();
        //   Adding role filter once table initialized
          this.api()
            .columns(4)
            .every(function () {
              var column = this;
              var select = $(
                '<select id="UserRole" class="form-control ml-50 text-capitalize"><option value=""> Pilih Kategori </option></select>'
              )
                .appendTo('.withdraw_status')
                .on('change', function () {
                  var val = $.fn.dataTable.util.escapeRegex($(this).val());
                  column.search(val ? '^' + val + '$' : '', true, false).draw();
                });
  
              column
                .data()
                .unique()
                .sort()
                .each(function (d, j) {
                  select.append('<option value="' + d + '" class="text-capitalize">' + d + '</option>');
                });
            });
        },
        drawCallback: function () {
          $(document).find('[data-toggle="tooltip"]').tooltip();
        }
      });
    }
  });
  