@extends('layouts/fullLayoutMaster')

@section('title', 'Register Page')

@section('vendor-style')
<link rel="stylesheet" href="{{ asset(mix('vendors/css/sweetalert/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v1 px-2">
  <div class="auth-inner py-2">
    <!-- Register v1 -->
    <div class="card mb-0">
      <div class="card-body">
        <a href="javascript:void(0);" class="brand-logo">
          <img class="round" src="{{asset('images/portrait/small/Minel_Tekno_Indonesia_Logo_Cropped.png')}}" alt="logo" height="65" width="65">
          <h1 class="brand-text text-primary ml-1 mt-1">MTI</h1>
        </a>

        <h4 class="card-title mb-1">Selamat datang di MTI-Commerce</h4>
        <p class="card-text mb-2">Silahkan daftarkan akun anda terlebih dahulu</p>

        <form action="{{ route('register') }}" method="post">
          @csrf
          @if(session('errors'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Something it's wrong:
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="form-group">
            <label for="register_name" class="form-label">Nama</label>
            <input type="text" class="form-control" id="register_name" name="register_name" placeholder="Buat Username" value="{{ old('register_name') }}" aria-describedby="register_name" tabindex="1" autofocus />
          </div>
          <div class="form-group">
            <label for="register_email" class="form-label">Email</label>
            <input type="text" class="form-control" id="register_email" name="register_email" placeholder="Masukkan Alamat Email Anda" value="{{ old('register_email') }}" aria-describedby="register_email" tabindex="2" />
          </div>
          <div class="form-group">
            <label for="register_phone" class="form-label">No. Handphone</label>
            <input type="text" class="form-control" id="register_phone" name="register_phone" placeholder="Masukkan No HP Anda" value="{{ old('register_phone') }}" aria-describedby="register_phone" tabindex="1" />
          </div>
          <div class="form-group">
            <label for="register_username" class="form-label">Username</label>
            <input type="text" class="form-control" id="register_username" name="register_username" placeholder="Masukkan Username Anda" value="{{ old('register_username') }}" aria-describedby="register_username" tabindex="1" />
          </div>
          <div class="form-group">
            <label for="register_password" class="form-label">Password</label>

            <div class="input-group input-group-merge form-password-toggle">
              <input type="password" class="form-control form-control-merge" id="register_password" name="register_password" placeholder="Buat Password" aria-describedby="register_password" tabindex="3" />
              <div class="input-group-append">
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="register_repeat_password" class="form-label">Konfirmasi Password</label>

            <div class="input-group input-group-merge form-password-toggle">
              <input type="password" class="form-control form-control-merge" id="register_password_confirmation" name="register_password_confirmation" placeholder="Ulangi Password" aria-describedby="register_password_confirmation" tabindex="3" />
            </div>
          </div>
          <!-- <div class="form-group">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" type="checkbox" id="register-privacy-policy" tabindex="4" />
              <label class="custom-control-label" for="register-privacy-policy">
                I agree to <a href="javascript:void(0);">privacy policy & terms</a>
              </label>
            </div>
          </div> -->
          <button type="submit" class="btn btn-primary btn-block mt-3" tabindex="5">Daftar</button>
        </form>

        <p class="text-center mt-2">
          <span>Sudah memiliki akun?</span>
          <a href="{{url('/')}}">
            <span>Halaman Login</span>
          </a>
        </p>
      </div>
    </div>
    <!-- /Register v1 -->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{ asset('vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
@endsection

@section('page-script')
<script src="{{ asset('js/scripts/pages/page-auth-register.js') }}"></script>
<script src="{{ asset(mix('js/scripts/sweetalert/sweetalert2.min.js')) }}"></script>

<script> //Buat Sweetalert
  $(document).ready(function() {

    @if(session()->has('errors'))
    Swal.fire({
      icon: 'error',
      title: 'Gagal menyimpan, mohon cek kembali',
    });
    @endif
  });
</script>

@endsection