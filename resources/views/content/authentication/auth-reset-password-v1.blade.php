@extends('layouts/fullLayoutMaster')

@section('title', 'Reset Password')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v1 px-2">
  <div class="auth-inner py-2">
    <!-- Reset Password v1 -->
    <div class="card mb-0">
      <div class="card-body">
        <a href="javascript:void(0);" class="brand-logo">
          <img class="round" src="{{asset('images/portrait/small/Minel_Tekno_Indonesia_Logo_Cropped.png')}}" alt="logo" height="65" width="65">
          <h1 class="brand-text text-primary ml-1 mt-1">MTI</h1>
        </a>

        <h4 class="card-title mb-1">Reset Password</h4>
        <p class="card-text mb-2">Halaman untuk reset ulang password anda. Langsung masukkan password baru anda pada kolom dibawah.</p>

        <form class="auth-reset-password-form mt-2" action="/auth/login-v1" method="POST">
          <div class="form-group">
            <div class="d-flex justify-content-between">
              <label for="reset-password-new">Password Baru</label>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
              <input type="password" class="form-control form-control-merge" id="reset-password-new" name="reset-password-new" placeholder="Masukkan Password Baru Anda" aria-describedby="reset-password-new" tabindex="1" autofocus />
              <div class="input-group-append">
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="d-flex justify-content-between">
              <label for="reset-password-confirm">Confirm Password</label>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
              <input type="password" class="form-control form-control-merge" id="reset-password-confirm" name="reset-password-confirm" placeholder="Konfirmasikan Password Baru Anda" aria-describedby="reset-password-confirm" tabindex="2" />
              <div class="input-group-append">
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
          </div>
          <button class="btn btn-primary btn-block" tabindex="3">Simpan Password Baru</button>
        </form>

        <p class="text-center mt-2">
          <a href="{{url('auth/login-v1')}}"> <i data-feather="chevron-left"></i> Kembali ke Login </a>
        </p>
      </div>
    </div>
    <!-- /Reset Password v1 -->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
<script src="{{asset(mix('js/scripts/pages/page-auth-reset-password.js'))}}"></script>
@endsection