@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('vendor-style')
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/sweetalert/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">

@endsection

@section('content')
<div class="auth-wrapper auth-v1 px-2">
  <div class="auth-inner py-2">
    <!-- Login v1 -->
    <div class="card mb-0">
      <div class="card-body">
        <!-- <button type="button" class="btn btn-outline-primary" id="basic-alert">Basic</button> -->
        <a href="javascript:void(0);" class="brand-logo">
          <img class="round" src="{{asset('images/portrait/small/Minel_Tekno_Indonesia_Logo_Cropped.png')}}" alt="logo" height="65" width="65">
          <h1 class="brand-text text-primary ml-1 mt-1">MTI</h1>
        </a>

        <h4 class="card-title mb-1">Selamat datang di MTI-Commerce</h4>
        <p class="card-text mb-2">Silahkan login ke akun anda</p>

        <form action="{{ route('login') }}" method="post">
          @csrf
          <!-- @if(session('errors'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Something it's wrong:
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif -->
          <!-- @if (Session::has('success'))
          <div class="alert alert-success">
            {{ Session::get('success') }}
          </div>
          @endif
          @if (Session::has('error'))
          <div class="alert alert-danger">
            {{ Session::get('error') }}
          </div>
          @endif -->
          <div class="form-group">
            <label for="login_email" class="form-label">Email</label>
            <input type="text" class="form-control" id="login_email" name="login_email" placeholder="Masukkan Email Anda" aria-describedby="login_email" tabindex="1" autofocus />
          </div>

          <div class="form-group">
            <div class="d-flex justify-content-between">
              <label for="login_password">Password</label>
              <a href="{{url('auth/forgot-password-v1')}}">
                <small>Lupa Password?</small>
              </a>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
              <input type="password" class="form-control form-control-merge" id="login_password" name="login_password" tabindex="2" placeholder="Masukkan Password Anda" aria-describedby="login_password" />
              <div class="input-group-append">
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
          </div>
          <!-- <div class="form-group">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" type="checkbox" id="remember-me" tabindex="3" />
              <label class="custom-control-label" for="remember-me"> Remember Me </label>
            </div>
          </div> -->
          <button class="btn btn-primary btn-block" tabindex="4">Masuk</button>
        </form>

        <p class="text-center mt-2">
          <span>Belum mendaftar?</span>
          <a href="{{url('register')}}">
            <span>Buat Akun</span>
          </a>
        </p>
      </div>
    </div>
    <!-- /Login v1 -->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
<script src="{{ asset(mix('js/scripts/pages/page-auth-login.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/sweetalert/sweetalert2.min.js')) }}"></script>

<script> //Buat Sweetalert
  $(document).ready(function() {

    @if(session()->has('success'))
    Swal.fire({
      icon: 'success',
      title: 'Berhasil!!',
      text: "{{ session()->get('success') }}"
    });
    @endif

    @if(session()->has('error'))
    Swal.fire({
      icon: 'error',
      title: 'Oopss!!',
      text: "{{ session()->get('error') }}"
    });
    @endif
  });
</script>

@endsection