@extends('layouts/detachedLayoutMaster')

@section('title', 'Lihat Event')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/base/pages/page-blog.css') }}" />
@endsection

@section('content-sidebar')
@include('/content/pages/page-blog-sidebar')
@endsection

@section('content')
<!-- Blog Detail -->
<div class="blog-detail-wrapper">
  <div class="row">
    <!-- Blog -->
    <div class="col-12">
      <div class="card">
        <img src="{{ asset('images/banner/banner-12.jpg') }}" class="img-fluid card-img-top" alt="Blog Detail Pic" />
        <div class="card-body">
          <h4 class="card-title">Judul Event 1</h4>
          <div class="media">
            <div class="avatar mr-50">
              <img src="{{ asset('images/portrait/small/avatar-s-7.jpg') }}" alt="Avatar" width="24" height="24" />
            </div>
            <div class="media-body">
              <small class="text-muted mr-25">by</small>
              <small><a href="javascript:void(0);" class="text-body">Ghani Pradita</a></small>
              <span class="text-muted ml-50 mr-25">|</span>
              <small class="text-muted">Jan 10, 2020</small>
            </div>
          </div>
          <div class="my-1 py-25">
            <a href="javascript:void(0);">
              <div class="badge badge-pill badge-light-danger mr-50">Invest</div>
            </a>
            <a href="javascript:void(0);">
              <div class="badge badge-pill badge-light-warning">Video</div>
            </a>
          </div>
          <p class="card-text mb-2">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vulputate orci in lacus pellentesque, consequat tempus nunc varius. In in purus condimentum, tincidunt erat vitae, finibus quam. Curabitur vulputate lorem et sapien venenatis ullamcorper. Sed a erat vitae dui rhoncus blandit. Praesent ut nisi felis. Integer at scelerisque nisl, et consequat velit. Aenean elit arcu, eleifend a neque vel, varius hendrerit nisi. Suspendisse potenti. Quisque eget felis at purus viverra aliquam quis in dolor. Morbi et urna venenatis, pulvinar augue ut, elementum quam. Pellentesque sed nunc dictum, faucibus nibh ac, dictum orci. Donec convallis facilisis sapien, at rhoncus tellus consectetur ac. Pellentesque euismod velit id condimentum ultrices. Nulla eget lacinia massa.
            <br />
            Fusce porttitor ex ut varius cursus. Proin neque velit, ultrices nec turpis sit amet, tristique malesuada tortor. In sed sapien in turpis fringilla interdum id sed libero. Curabitur pellentesque volutpat cursus. Donec lectus libero, lobortis in est vitae, fermentum accumsan turpis. Donec pretium maximus purus, id venenatis odio feugiat bibendum. Phasellus quis volutpat arcu. Phasellus vulputate, enim a mattis vestibulum, sapien ex pulvinar nibh, a efficitur diam neque vitae mi. Duis aliquam tortor id libero porta, eget porta massa finibus. Maecenas blandit non urna in porta. Donec suscipit vestibulum nisl, condimentum congue lectus laoreet ut.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!--/ Blog -->

  <!-- Leave a Blog Comment -->
  <div class="col-12">
    <h6 class="section-label">Tulis Komentar</h6>
    <div class="card">
      <div class="card-body">
        <form action="javascript:void(0);" class="form">
          <div class="row">
            <div class="col-sm-6 col-12">
              <div class="form-group mb-2">
                <input type="text" class="form-control" placeholder="Nama Anda" />
              </div>
            </div>
            <div class="col-sm-6 col-12">
              <div class="form-group mb-2">
                <input type="email" class="form-control" placeholder="Email Anda" />
              </div>
            </div>
            <!-- <div class="col-sm-6 col-12">
              <div class="form-group mb-2">
                <input type="url" class="form-control" placeholder="Website" />
              </div>
            </div> -->
            <div class="col-12">
              <textarea class="form-control mb-2" rows="4" placeholder="Tulis Komentar Disini..."></textarea>
            </div>
            <!-- <div class="col-12">
              <div class="custom-control custom-checkbox mb-2">
                <input type="checkbox" class="custom-control-input" id="blogCheckbox" />
                <label class="custom-control-label" for="blogCheckbox">Save my name, email, and website in this browser for the next time I comment.</label>
              </div>
            </div> -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Kirim Komentar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--/ Leave a Blog Comment -->
</div>
</div>
<!--/ Blog Detail -->
@endsection