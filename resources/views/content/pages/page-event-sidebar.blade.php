<div class="blog-sidebar right-sidebar my-2 my-lg-0">
  <div class="right-sidebar-content">
    <!-- Search bar -->
    <div class="blog-search">
      <div class="input-group input-group-merge">
        <input type="text" class="form-control" placeholder="Search here" />
        <div class="input-group-append">
          <span class="input-group-text cursor-pointer">
            <i data-feather="search"></i>
          </span>
        </div>
      </div>
    </div>
    <!--/ Search bar -->

    <!-- Recent Posts -->
    <div class="blog-recent-posts mt-3">
      <h6 class="section-label">Event Terkini</h6>
      <div class="mt-75">
        <div class="media mb-2">
          <a href="{{asset('page/blog/detail')}}" class="mr-2">
            <img
              class="rounded"
              src="{{asset('images/banner/banner-22.jpg')}}"
              width="100"
              height="70"
              alt="Recent Post Pic"
            />
          </a>
          <div class="media-body">
            <h6 class="blog-recent-post-title">
              <a href="{{asset('page/blog/detail')}}" class="text-body-heading">Event Terkini 1</a>
            </h6>
            <div class="text-muted mb-0">Jan 14 2020</div>
          </div>
        </div>
        <div class="media mb-2">
          <a href="{{asset('page/blog/detail')}}" class="mr-2">
            <img
              class="rounded"
              src="{{asset('images/banner/banner-27.jpg')}}"
              width="100"
              height="70"
              alt="Recent Post Pic"
            />
          </a>
          <div class="media-body">
            <h6 class="blog-recent-post-title">
              <a href="{{asset('page/blog/detail')}}" class="text-body-heading">Event Terkini 2</a>
            </h6>
            <div class="text-muted mb-0">Mar 04 2020</div>
          </div>
        </div>
        <div class="media mb-2">
          <a href="{{asset('page/blog/detail')}}" class="mr-2">
            <img
              class="rounded"
              src="{{asset('images/banner/banner-39.jpg')}}"
              width="100"
              height="70"
              alt="Recent Post Pic"
            />
          </a>
          <div class="media-body">
            <h6 class="blog-recent-post-title">
              <a href="{{asset('page/blog/detail')}}" class="text-body-heading">Event Terkini 3</a>
            </h6>
            <div class="text-muted mb-0">Feb 18 2020</div>
          </div>
        </div>
        <div class="media">
          <a href="{{asset('page/blog/detail')}}" class="mr-2">
            <img
              class="rounded"
              src="{{asset('images/banner/banner-35.jpg')}}"
              width="100"
              height="70"
              alt="Recent Post Pic"
            />
          </a>
          <div class="media-body">
            <h6 class="blog-recent-post-title">
              <a href="{{asset('page/blog/detail')}}" class="text-body-heading">Event Terkini 4</a>
            </h6>
            <div class="text-muted mb-0">Oct 08 2020</div>
          </div>
        </div>
      </div>
    </div>
    <!--/ Recent Posts -->

  </div>
</div>
