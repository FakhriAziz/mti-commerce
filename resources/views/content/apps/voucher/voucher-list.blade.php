@extends('layouts/contentLayoutMaster')

@section('title', 'Voucher List')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap.min.css')}}">
@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Daftar Voucher Anda</h4>
    </div>
    <div class="card-body">
      <div class="card-datatable table-responsive">
        <table class="voucher-list-table table">
          <thead>
            <tr>
              <th>Kode Voucher</th>
              <th class="text-truncate">Status Voucher</th>
              <th class="text-truncate">Tanggal Diterima</th>
              <th class="text-truncate">Status Voucher</th> <!--hidden untuk filter-->
              <th class="cell-fit">Actions</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/app-voucher-list.js')}}"></script>
@endsection