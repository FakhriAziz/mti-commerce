@extends('layouts/contentLayoutMaster')

@section('title', 'Bonus List')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap.min.css')}}">
@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Daftar Bonus Anda</h4>
    </div>
    <div class="card-body">
      <div class="card-datatable table-responsive">
        <table class="bonus-list-table table">
          <thead>
          <tr>
              <th>Kode Bonus</th>
              <th class="text-truncate">Tanggal Diterima</th>
              <th class="text-truncate">Bonus Dari</th>
              <th class="text-truncate">Jumlah</th>
              <!-- <th class="text-truncate">Status Voucher</th> hidden untuk filter -->
              <th class="cell-fit">Actions</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap.min.js')}}"></script>
@endsection

@section('page-script')
<!-- <script src="{{asset('js/scripts/pages/app-bonus-list.js')}}"></script> -->
<script>

$(function () {
    'use strict';

    var tableData = {!! $data->toJson() !!};
  
    var dtBonusTable = $('.bonus-list-table'),
      assetPath = '../../../app-assets/',
      bonusPreview = 'app-bonus-preview.html',
      bonusAdd = 'app-bonus-buy.html',
      bonusEdit = 'app-bonus-edit.html';
  
    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
      bonusPreview = assetPath + 'app/bonus/preview';
      bonusAdd = assetPath + 'app/bonus/buy';
      bonusEdit = assetPath + 'app/bonus/edit';
    }
  
    // datatable
    if (dtBonusTable.length) {
      var dtBonus = dtBonusTable.DataTable({
        // ajax: assetPath + 'data/bonus-list.json', // JSON file to add data
        data: tableData,
        autoWidth: false,
        columns: [
          // columns according to JSON
          { data: 'bonus_id' },
          { data: 'issued_date' },
          { data: 'bonus_from' },
          { data: 'total' },
          { data: '' }
        ],
        columnDefs: [
          {
            // Voucher ID
            targets: 0,
            width: '150px',
            render: function (data, type, full, meta) {
              var $bonusId = full['bonus_id'];
              // Creates full output for row
              var $rowOutput = '<a class="font-weight-bold" href="' + bonusPreview + '"> #' + $bonusId + '</a>';
              return $rowOutput;
            }
          },
          {
            // Total Invoice Amount
            targets: 3,
            width: '73px',
            render: function (data, type, full, meta) {
              var $total = full['total'];
              return '<span class="d-none">' + $total + '</span>Rp.' + $total;
            }
          },
          {
            // Actions
            targets: -1,
            title: 'Actions',
            width: '80px',
            orderable: false,
            render: function (data, type, full, meta) {
              return (
                '<div class="d-flex align-items-center col-actions">' +
                '<a class="mr-1" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Send Mail">' +
                feather.icons['send'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<a class="mr-1" href="' +
                bonusPreview +
                '" data-toggle="tooltip" data-placement="top" title="Preview Bonus">' +
                feather.icons['eye'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<div class="dropdown">' +
                '<a class="btn btn-sm btn-icon px-0" data-toggle="dropdown">' +
                feather.icons['more-vertical'].toSvg({ class: 'font-medium-2' }) +
                '</a>' +
                '<div class="dropdown-menu dropdown-menu-right">' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['download'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Download</a>' +
                '<a href="' +
                bonusEdit +
                '" class="dropdown-item">' +
                feather.icons['edit'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Edit</a>' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['trash'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Delete</a>' +
                '<a href="javascript:void(0);" class="dropdown-item">' +
                feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) +
                'Duplicate</a>' +
                '</div>' +
                '</div>' +
                '</div>'
              );
            }
          }
        ],
        order: [[1, 'desc']],
        dom:
          '<"row d-flex justify-content-between align-items-center m-1"' +
          '<"col-lg-6 d-flex align-items-center"l<"dt-action-buttons text-xl-right text-lg-left text-lg-right text-left "B>>' +
          '<"col-lg-6 d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap pr-lg-1 p-0"f<"voucher_status ml-sm-2">>' +
          '>t' +
          '<"d-flex justify-content-between mx-2 row"' +
          '<"col-sm-12 col-md-6"i>' +
          '<"col-sm-12 col-md-6"p>' +
          '>',
        language: {
          sLengthMenu: 'Show _MENU_',
          search: 'Cari',
          searchPlaceholder: 'Cari Data',
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        },
        // Buttons with Dropdown
        buttons: [
          {
            text: 'Beli Voucher',
            className: 'btn btn-primary btn-add-record ml-2',
            // action: function (e, dt, button, config) {
            //   window.location = voucherAdd;
            // }
          }
        ],
        // For responsive popup
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Details of ' + data['client_name'];
              }
            }),
            type: 'column',
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: 'table',
              columnDefs: [
                {
                  targets: 2,
                  visible: false
                },
                {
                  targets: 3,
                  visible: false
                }
              ]
            })
          }
        },
        initComplete: function () {
          $(document).find('[data-toggle="tooltip"]').tooltip();
        //   Adding role filter once table initialized
          this.api()
            .columns(2)
            .every(function () {
              var column = this;
              var select = $(
                '<select id="UserRole" class="form-control ml-50 text-capitalize"><option value=""> Pilih Kategori </option></select>'
              )
                .appendTo('.bonus_from')
                .on('change', function () {
                  var val = $.fn.dataTable.util.escapeRegex($(this).val());
                  column.search(val ? '^' + val + '$' : '', true, false).draw();
                });
  
              column
                .data()
                .unique()
                .sort()
                .each(function (d, j) {
                  select.append('<option value="' + d + '" class="text-capitalize">' + d + '</option>');
                });
            });
        },
        drawCallback: function () {
          $(document).find('[data-toggle="tooltip"]').tooltip();
        }
      });
    }
  });

</script>
@endsection