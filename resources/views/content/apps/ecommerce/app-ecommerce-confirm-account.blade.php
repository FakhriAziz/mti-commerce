@extends('layouts/contentLayoutMaster')

@section('title', 'Konfirmasi Akun')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">

@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Halaman Konfirmasi Akun</h4>
        </div>
        <div class="card-body">
            <!-- Basic Select -->
            <form class="form form-horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-sm-2 col-form-label">
                                <label for="transfer-username">Broker</label>
                            </div>
                            <div class="col-sm-10">
                                <select class="select2 form-control form-control-lg" id="transfer-username">
                                    <option>Broker1</option>
                                    <option>Broker2</option>
                                    <option>Broker3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-sm-2 col-form-label">
                                <label for="transfer-jumlah">Nomer Akun</label>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group input-group-md">
                                    <input type="text" class="form-control" id="login-email" name="login-email" placeholder="Masukkan Nomer Akun Anda" aria-describedby="login-email" tabindex="1" autofocus />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="reset" class="btn btn-primary mr-1">Konfirmasi</button>
                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap.min.js')}}"></script>
<script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice-list.js')}}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-number-input.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>

@endsection