@extends('layouts/contentLayoutMaster')

@section('title', 'Checkout')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection

@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-number-input.css')) }}">
@endsection
@section('content')
<div class="bs-stepper checkout-tab-steps">
  <!-- Wizard starts -->
  <div class="bs-stepper-header">
    <div class="step" data-target="#step-cart">
      <button type="button" class="step-trigger">
        <span class="bs-stepper-box">
          <i data-feather="shopping-cart" class="font-medium-3"></i>
        </span>
        <span class="bs-stepper-label">
          <span class="bs-stepper-title">Cart</span>
          <span class="bs-stepper-subtitle">Keranjang Anda</span>
        </span>
      </button>
    </div>
    <!-- <div class="line">
      <i data-feather="chevron-right" class="font-medium-2"></i>
    </div>
    <div class="step" data-target="#step-address">
      <button type="button" class="step-trigger">
        <span class="bs-stepper-box">
          <i data-feather="home" class="font-medium-3"></i>
        </span>
        <span class="bs-stepper-label">
          <span class="bs-stepper-title">Address</span>
          <span class="bs-stepper-subtitle">Enter Your Address</span>
        </span>
      </button>
    </div> -->
    <div class="line">
      <i data-feather="chevron-right" class="font-medium-2"></i>
    </div>
    <div class="step" data-target="#step-payment">
      <button type="button" class="step-trigger">
        <span class="bs-stepper-box">
          <i data-feather="credit-card" class="font-medium-3"></i>
        </span>
        <span class="bs-stepper-label">
          <span class="bs-stepper-title">Payment</span>
          <span class="bs-stepper-subtitle">Pilih Metode Pembayaran</span>
        </span>
      </button>
    </div>
  </div>
  <!-- Wizard ends -->

  <div class="bs-stepper-content">
    <!-- Checkout Place order starts -->
    <div id="step-cart" class="content">
      <div id="place-order" class="list-view product-checkout">
        <!-- Checkout Place Order Left starts -->
        <div class="checkout-items">
          <div class="card ecommerce-card">
            <div class="item-img">
              <a href="{{url('app/ecommerce/details')}}">
                <img src="{{ asset('images/portrait/small/Minel_Tekno_Indonesia_Logo_Cropped.png') }}" alt="img-placeholder" />
              </a>
            </div>
            <div class="card-body">
              <div class="item-name">
                <h6 class="mb-0"><a href="{{url('app/ecommerce/details')}}" class="text-body">Voucher Trading MTI</a></h6>
                <!-- <span class="item-company">By <a href="javascript:void(0)" class="company-name">Apple</a></span> -->
                <!-- <div class="item-rating">
                  <ul class="unstyled-list list-inline">
                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                    <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                  </ul>
                </div> -->
              </div>
              <span class="text-success mb-1">Ready Stock</span>
              <div class="item-quantity">
                <span class="quantity-title">Qty:</span>
                <div class="input-group quantity-counter-wrapper">
                  <input type="text" class="quantity-counter" value="1" />
                </div>
              </div>
              <!-- <span class="delivery-date text-muted">Delivery by, Wed Apr 25</span>
              <span class="text-success">17% off 4 offers Available</span> -->
            </div>
            <div class="item-options text-center">
              <div class="item-wrapper">
                <div class="item-cost">
                  <h4 class="item-price">Rp. 1.000.000,00</h4>
                  <!-- <p class="card-text shipping">
                    <span class="badge badge-pill badge-light-success">Free Shipping</span>
                  </p> -->
                </div>
              </div>
              <button type="button" class="btn btn-light mt-1 remove-wishlist">
                <i data-feather="x" class="align-middle mr-25"></i>
                <span>Remove</span>
              </button>
              <button type="button" class="btn btn-primary btn-cart move-cart">
                <i data-feather="heart" class="align-middle mr-25"></i>
                <span class="text-truncate">Add to Wishlist</span>
              </button>
            </div>
          </div>
        </div>
        <!-- Checkout Place Order Left ends -->

        <!-- Checkout Place Order Right starts -->
        <div class="checkout-options">
          <div class="card">
            <div class="card-body">
              <label class="section-label mb-1">Pilihan</label>
              <div class="coupons input-group input-group-merge">
                <input type="text" class="form-control" placeholder="Kupon" aria-label="Kode Kupon" aria-describedby="input-coupons" />
                <div class="input-group-append">
                  <span class="input-group-text text-primary" id="input-coupons">Masukkan Kode</span>
                </div>
              </div>
              <hr />
              <div class="price-details">
                <h6 class="price-title">Detail Harga</h6>
                <ul class="list-unstyled">
                  <li class="price-detail">
                    <div class="detail-title">Total Voucher</div>
                    <div class="detail-amt">Rp. 1.000.000,00</div>
                  </li>
                  <li class="price-detail">
                    <div class="detail-title">Diskon</div>
                    <div class="detail-amt discount-amt text-success">-25$</div>
                  </li>
                  <!-- <li class="price-detail">
                    <div class="detail-title">Estimated Tax</div>
                    <div class="detail-amt">$1.3</div>
                  </li>
                  <li class="price-detail">
                    <div class="detail-title">EMI Eligibility</div>
                    <a href="javascript:void(0)" class="detail-amt text-primary">Details</a>
                  </li>
                  <li class="price-detail">
                    <div class="detail-title">Delivery Charges</div>
                    <div class="detail-amt discount-amt text-success">Free</div>
                  </li> -->
                </ul>
                <hr />
                <ul class="list-unstyled">
                  <li class="price-detail">
                    <div class="detail-title detail-total">Total</div>
                    <div class="detail-amt font-weight-bolder">Rp. 750.000,00</div>
                  </li>
                </ul>
                <button type="button" class="btn btn-primary btn-block btn-next place-order">Lanjut Metode Pembayaran</button>
              </div>
            </div>
          </div>
          <!-- Checkout Place Order Right ends -->
        </div>
      </div>
      <!-- Checkout Place order Ends -->
    </div>

    <!-- Checkout Payment Starts -->
    <div id="step-payment" class="content">
      <form id="checkout-payment" class="list-view product-checkout" onsubmit="return false;">
        <div class="payment-type">
          <div class="card">
            <div class="card-header flex-column align-items-start">
              <h4 class="card-title">Opsi Pembayaran</h4>
              <p class="card-text text-muted mt-25">Pilih salah satu metode pembayaran dibawah ini.</p>
            </div>
            <div class="card-body">
              <h6 class="card-holder-name my-75">A.n PT. Minel Tekno Indonesia</h6>
              <!-- <div class="custom-control custom-radio">
                <input type="radio" id="customColorRadio1" name="paymentOptions" class="custom-control-input" checked />
                <label class="custom-control-label" for="customColorRadio1">
                  Unlocked Debit Card 12XX XXXX XXXX 0000
                </label>
              </div>
              <div class="customer-cvv mt-1">
                <div class="form-inline">
                  <label class="mb-50" for="card-holder-cvv">Enter CVV:</label>
                  <input type="password" class="form-control ml-sm-75 ml-0 mb-50 input-cvv" name="input-cvv" id="card-holder-cvv" />
                  <button type="button" class="btn btn-primary btn-cvv ml-0 ml-sm-1 mb-50">Lanjutkan</button>
                </div>
              </div> -->
              <hr class="my-2" />
              <ul class="other-payment-options list-unstyled">
                <li class="py-50">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customColorRadio2" name="paymentOptions" class="custom-control-input" />
                    <label class="custom-control-label" for="customColorRadio2"> Credit / Debit / ATM Card </label>
                  </div>
                </li>
                <li class="py-50">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customColorRadio3" name="paymentOptions" class="custom-control-input" />
                    <label class="custom-control-label" for="customColorRadio3"> Net Banking </label>
                  </div>
                </li>
                <li class="py-50">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customColorRadio4" name="paymentOptions" class="custom-control-input" />
                    <label class="custom-control-label" for="customColorRadio4"> EMI (Easy Installment) </label>
                  </div>
                </li>
                <li class="py-50">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="customColorRadio5" name="paymentOptions" class="custom-control-input" />
                    <label class="custom-control-label" for="customColorRadio5"> Cash On Delivery </label>
                  </div>
                </li>
              </ul>
              <!-- <hr class="my-2" />
              <div class="gift-card mb-25">
                <p class="card-text">
                  <i data-feather="plus-circle" class="mr-50 font-medium-5"></i>
                  <span class="align-middle">Add Gift Card</span>
                </p>
              </div> -->
            </div>
          </div>
        </div>
        <div class="amount-payable checkout-options">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Price Details</h4>
            </div>
            <div class="card-body">
              <ul class="list-unstyled price-details">
                <li class="price-detail">
                  <div class="details-title">Harga</div>
                  <div class="detail-amt">
                    <strong>Rp. 750.000,00</strong>
                  </div>
                </li>
                <!-- <li class="price-detail">
                  <div class="details-title">Delivery Charges</div>
                  <div class="detail-amt discount-amt text-success">Free</div>
                </li> -->
              </ul>
              <hr />
              <ul class="list-unstyled price-details">
                <li class="price-detail">
                  <div class="details-title">Nilai Yang Dibayarkan</div>
                  <div class="detail-amt font-weight-bolder">Rp. 750.000,00</div>
                </li>
              </ul>
              <a type="button" class="btn btn-primary btn-block btn-next place-order" href="{{url('app/invoice/preview')}}" >Konfirmasi Pembayaran</a>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- Checkout Payment Ends -->
    <!-- </div> -->
  </div>
</div>

@endsection

@section('vendor-script')
<!-- Vendor js files -->
<script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/app-ecommerce-checkout.js')) }}"></script>
@endsection