
@extends('layouts/contentLayoutMaster')

@section('title', 'Price List')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/base/pages/page-pricing.css')}}">
@endsection

@section('content')
<section id="pricing-plan">
  <!-- title text and switch button -->
  <div class="text-center">
    <h1 class="mt-5">Halaman Tutorial</h1>
    <p class="mb-2 pb-75">
      Panduan cara menggunakan Robot Trading (Copy Trade).
    </p>
    <!-- <div class="d-flex align-items-center justify-content-center mb-5 pb-50">
      <h6 class="mr-1 mb-0">Monthly</h6>
      <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="priceSwitch" />
        <label class="custom-control-label" for="priceSwitch"></label>
      </div>
      <h6 class="ml-50 mb-0">Annually</h6>
    </div> -->
  </div>
  <!--/ title text and switch button -->

  <!-- pricing plan cards -->
  <div class="row pricing-card">
    <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto">
      <div class="row">

        <div class="col-12 col-md-4">
          <div class="card basic-pricing text-center" style="height:400px">
            <div class="card-body">
              <img src="{{asset('images/illustration/robot-de-trading-1024x640-1.jpg')}}" class="mb-2 mt-2" width="160px" />
              <h2>Daftar Akun</h2>
              <p class="card-text">Daftar akun trading melalui Broker Xsocio Market dibawah ini :</p>
              <a type="button" class="btn btn-block btn-primary mt-2" href="#">Daftar</a>
            </div>
          </div>
        </div>

        <div class="col-12 col-md-4">
          <div class="card basic-pricing text-center" style="height:400px">
            <div class="card-body">
              <img src="{{asset('images/illustration/robot-de-trading-1024x640-1.jpg')}}" class="mb-2 mt-2" width="160px" />
              <h2>Deposit Akun</h2>
              <p class="card-text">Deposit dana anda ke akun yang sudah dibuat di Broker Xsocio Market minimal deposit $100.</p>
              <!-- <a type="button" class="btn btn-block btn-primary mt-2" href="#">Daftar</a> -->
            </div>
          </div>
        </div>

        <div class="col-12 col-md-4">
          <div class="card basic-pricing text-center" style="height:400px">
            <div class="card-body">
              <img src="{{asset('images/illustration/robot-de-trading-1024x640-1.jpg')}}" class="mb-2 mt-2" width="160px" />
              <h2>Follow Akun</h2>
              <p class="card-text">Follow akun pilot kami di Broker Xsocio Market dengan klik tombol dibawah ini :</p>
              <a type="button" class="btn btn-block btn-primary mt-2" href="#">Follow Robot 1</a>
              <a type="button" class="btn btn-block btn-primary mt-2" href="#">Follow Robot 2</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!--/ pricing plan cards -->

</section>
@endsection

@section('page-script')
{{-- Page js files --}}
<script src="{{asset('js/scripts/pages/page-pricing.js')}}"></script>
@endsection
