<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendEmail;

class MailController extends Controller
{
    public function index()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('content/testing/email', ['pageConfigs' => $pageConfigs]);
    }

    public function sendEmail()
    {
        Mail::to("penerima@gmail.com")->send(new SendEmail());
 
		return "Email telah dikirim";
    }
    // public function sendEmail(Request $request) //with Form
    // {
    //     $this->validate($request, [
    //         "email" => "required",
    //         "subject" => "required",
    //         "message" => "required"
    //     ]);
    //     $email = $request->email;
    //     $subject = $request->subject;
    //     $message = $request->message;

    //     Mail::to($email)->send(new SendEmail($subject, $message));
    // }
}
