<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
  // Home - Landing Page - FAF has added
  public function landingPage()
  {
    // $pageConfigs = ['pageHeader' => false];

    return view('/content/home/landing-page');
  }

  
}
