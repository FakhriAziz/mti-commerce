<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bonus;

class AppsController extends Controller
{
  // bonus list App by FAF
  public function bonus_list()
  {
    $data = Bonus::all();
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/bonus/bonus-list', ['pageConfigs' => $pageConfigs], ['data' => $data]);
  }

  // withdraw list App by FAF
  public function withdraw_list()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/bonus/withdraw-list', ['pageConfigs' => $pageConfigs]);
  }

  // voucher list App by FAF
  public function voucher_list()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/voucher/voucher-list', ['pageConfigs' => $pageConfigs]);
  }

  // voucher transfer App by FAF
  public function voucher_transfer()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/voucher/voucher-transfer', ['pageConfigs' => $pageConfigs]);
  }

  // voucher buy App by FAF
  public function voucher_buy()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/voucher/voucher-buy', ['pageConfigs' => $pageConfigs]);
  }

  // voucher list App by FAF
  public function voucher_new_member()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/voucher/voucher-new-member', ['pageConfigs' => $pageConfigs]);
  }

  // invoice list App
  public function invoice_list()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-list', ['pageConfigs' => $pageConfigs]);
  }

  // invoice preview App
  public function invoice_preview()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-preview', ['pageConfigs' => $pageConfigs]);
  }

  // invoice edit App
  public function invoice_edit()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-edit', ['pageConfigs' => $pageConfigs]);
  }

  // invoice edit App
  public function invoice_add()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-add', ['pageConfigs' => $pageConfigs]);
  }

  // invoice print App
  public function invoice_print()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/apps/invoice/invoice-print', ['pageConfigs' => $pageConfigs]);
  }

  // User List Page
  public function user_list()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/user/app-user-list', ['pageConfigs' => $pageConfigs]);
  }

  // User View Page
  public function user_view()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/user/app-user-view', ['pageConfigs' => $pageConfigs]);
  }

  // User Edit Page
  public function user_edit()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/user/app-user-edit', ['pageConfigs' => $pageConfigs]);
  }

  // Ecommerce Tutorial
  public function ecommerce_tutorial()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/ecommerce/app-ecommerce-tutorial', ['pageConfigs' => $pageConfigs]);
  }

  // Ecommerce Price
  public function ecommerce_confirm_account()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/ecommerce/app-ecommerce-confirm-account', ['pageConfigs' => $pageConfigs]);
  }

  // Ecommerce Price
  public function ecommerce_price()
  {
    $pageConfigs = ['pageHeader' => false];
    return view('/content/apps/ecommerce/app-ecommerce-price', ['pageConfigs' => $pageConfigs]);
  }

  // Ecommerce Shop
  public function ecommerce_shop()
  {
    $pageConfigs = [
      'contentLayout' => "content-detached-left-sidebar",
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Shop"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-shop', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Ecommerce Details
  public function ecommerce_details()
  {
    $pageConfigs = [
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['link' => "/app/ecommerce/shop", 'name' => "Shop"], ['name' => "Details"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-details', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Ecommerce Checkout
  public function ecommerce_checkout()
  {
    $pageConfigs = [
      'pageClass' => 'ecommerce-application',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
    ];

    return view('/content/apps/ecommerce/app-ecommerce-checkout', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }
}
