<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;

class AuthController extends Controller
{
  public function showFormLogin()
  {
    if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
      //Login Success
      return redirect('/content/dashboard/dashboard-analytics');
    }
    $pageConfigs = ['blankPage' => true];

    return view('/content/authentication/auth-login-v1', ['pageConfigs' => $pageConfigs]);
  }

  public function login(Request $request)
  {
    $rules = [
      'login_email'                 => 'required|email',
      'login_password'              => 'required|string'
    ];

    $messages = [
      'login_email.required'        => 'Email wajib diisi',
      'login_email.email'           => 'Email tidak valid',
      'login_password.required'     => 'Password wajib diisi',
      'login_password.string'       => 'Password harus berupa angka atau huruf'
    ];

    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput($request->all);
    }

    $data = [
      'email'     => $request->input('login_email'),
      'password'  => $request->input('login_password'),
    ];

    Auth::attempt($data);

    if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
      //Login Success
      return redirect()->route('dashboard');
    } else { // false

      //Login Fail
      $pageConfigs = ['blankPage' => true];

      Session::flash('error', 'Email atau password salah');
      return redirect()->route('login');
    }
  }

  public function showFormRegister()
  {
    $pageConfigs = ['blankPage' => true];

    return view('/content/authentication/auth-register-v1', ['pageConfigs' => $pageConfigs]);
  }

  public function register(Request $request)
  {
    // dd($request);
    $rules = [
      'register_name'                  => 'required|min:3|max:35',
      'register_username'              => 'required|min:3|max:35|unique:users,username',
      'register_phone'                 => 'required|min:3|max:35',
      'register_email'                 => 'required|email|unique:users,email',
      'register_password'              => 'required|confirmed'
    ];

    $messages = [
      'register_name.required'         => 'Nama Lengkap wajib diisi',
      'register_name.min'              => 'Nama lengkap minimal 3 karakter',
      'register_name.max'              => 'Nama lengkap maksimal 35 karakter',

      'register_username.required'     => 'Username wajib diisi',
      'register_username.min'          => 'Username minimal 3 karakter',
      'register_username.max'          => 'Username maksimal 35 karakter',
      'register_username.unique'       => 'Username sudah terdaftar',

      'register_phone.required'        => 'No. Telepon wajib diisi',
      'register_phone.min'             => 'No. Telepon minimal 3 karakter',
      'register_phone.max'             => 'No. Telepon maksimal 35 karakter',

      'register_email.required'        => 'Email wajib diisi',
      'register_email.email'           => 'Email tidak valid',
      'register_email.unique'          => 'Email sudah terdaftar',

      'register_password.required'     => 'Password wajib diisi',
      'register_password.confirmed'    => 'Password tidak sama dengan konfirmasi password'
    ];

    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput($request->all);
    }

    $user = new User;
    $user->name = ucwords(strtolower($request->register_name));
    $user->email = strtolower($request->register_email);
    $user->no_phone = strtolower($request->register_phone);
    $user->username = strtolower($request->register_username);
    $user->password = Hash::make($request->register_password);
    $user->email_verified_at = \Carbon\Carbon::now();
    $simpan = $user->save();

    if ($simpan) {
      Session::flash('success', 'Register berhasil! Silahkan login untuk mengakses data');
      return redirect()->route('login');
    } else {
      Session::flash('errors', ['' => 'Register gagal! Mohon menghubungi admin']);
      return redirect()->route('register');
    }
  }

  public function logout()
  {
    Auth::logout(); // menghapus session yang aktif
    return redirect()->route('login');
  }
}
